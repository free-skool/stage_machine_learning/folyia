# folyia

Code pour un camps d'été à destination des lycéennes. 

## Installation mode facile

+ Installer [Anaconda pour Python 3.7](https://www.anaconda.com/distribution/)
+ Ouvrir la console Anaconda Prompt (pour être certain d'utiliser l'interpréteur Python d'Anaconda)
+ Installer le package Python [face recognition](https://pypi.org/project/face_recognition/)

```shell
pip install face_recognition
```
+ Installer Keras et Tensorflow 
```shell
conda install keras tensorflow
```

**Attention :** cette installation va prendre plusieurs minutes.

+ Ouvrir Jupyter Notebook
+ Ouvrir le fichier **test_visage_segmentatio_on_video.ipynb**


