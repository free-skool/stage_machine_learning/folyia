import face_recognition
import numpy as np
import cv2


def f1(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    return cv2.cvtColor(gray,  cv2.COLOR_GRAY2BGR)


def f2(image):
    res = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _,res = cv2.threshold(res,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    return cv2.cvtColor(res,  cv2.COLOR_GRAY2BGR)


def f3(image):
    res = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    res[:,:, 1] = 255

    return cv2.cvtColor(res,  cv2.COLOR_HSV2BGR)


def f4(image):
    res = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    red = res[:,:,0].copy()
    res[:,:,0] = res[:,:,1]
    res[:,:,1] = red
    
    return cv2.cvtColor(res,  cv2.COLOR_RGB2BGR)


def connecter_camera():

    return cv2.VideoCapture(0)


def capturer_image(cam):
    ret, frame = cam.read()
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    return image


def ecouter_clavier():

    return chr(cv2.waitKey(1) & 0xFF)


def localiser_visages(image):
    return face_recognition.face_locations(image)


def encadrer_visages(image, rect):

    haut, droite, bas, gauche = rect
    width = droite - gauche
    height = bas - haut
    p1 = (gauche, haut)
    p2 = (droite, bas)
    
    cv2.rectangle(image, p1, p2, (255, 0, 255), thickness=3)


def charger_masque(chemin_image):
    masque = cv2.imread(chemin_image, cv2.IMREAD_LOAD_GDAL)

    return cv2.cvtColor(masque, cv2.COLOR_BGRA2RGBA)


def couper_connexion(cam):
    cam.release()
    cv2.destroyAllWindows()


def afficher_image(image):
    cv2.imshow('masque', cv2.cvtColor(image, cv2.COLOR_RGBA2BGRA))


def combiner_images(image1, image2, rect):
    haut, droite, bas, gauche = rect
    haut = max(haut, 0)
    gauche = max(gauche, 0)
    bas = min(bas, image1.shape[0]-1)
    droite = min(droite, image1.shape[1]-1)
    width = droite - gauche
    height = bas - haut
    image2 = cv2.resize(image2, (width, height))
    pixels_opaques = image2[:, :, -1] > 0
    image1[haut:bas, gauche:droite, :][pixels_opaques] = image2[:, :, :3][pixels_opaques]

    return image1


def sauvegarder_image(image):
    cv2.imwrite('./capture.png', image)
