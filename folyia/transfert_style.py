from __future__ import print_function
from keras.preprocessing.image import load_img, save_img, img_to_array
import numpy as np
from scipy.optimize import fmin_l_bfgs_b
from keras.applications import vgg19
from keras import backend as ts
from matplotlib import pyplot as plt

from keras.applications.vgg19 import VGG19, preprocess_input, decode_predictions
from keras.preprocessing.image import load_img, img_to_array
import numpy as np


def calculer_determinant_de_gram(img):
    features = ts.batch_flatten(ts.permute_dimensions(img, (2, 0, 1)))
    gram = ts.dot(features, ts.transpose(features))
    return gram


def derivee_suivant_x(img, nombre_lignes, nombre_colonnes):
    return ts.square(
        img[:, :nombre_lignes - 1, :nombre_colonnes - 1, :]
        - img[:, 1:, :nombre_colonnes - 1, :]
        )


def derivee_suivant_y(img, nombre_lignes, nombre_colonnes):
    return ts.square(
        img[:, :nombre_lignes - 1, :nombre_colonnes - 1, :]
        - img[:, :nombre_lignes - 1, 1:, :]
        )

def somme(img):
    return ts.sum(img)

def au_carre(img):
    return ts.square(img)

def puissance(img, exposant):
    return ts.pow(img, exposant)


def charger_model():
   return VGG19(weights='imagenet')

def afficher_model(model):
    print(model.summary())
    
def charger_image(src_image_path):  

    # lire une image
    img = load_img(src_image_path, target_size=(224, 224))
    print('Image en mémoire')

    # la convertir en un tableau de nombre
    img = img_to_array(img)
    print('Image convertie en tableau de nombre')

    # la préparer pour VGG19
    img = preprocess_input(img)
    print('Image préparée pour le model')

    # creer une liste contenant 
    liste_images = np.expand_dims(img, axis=0)
    
    return liste_images

def analyser_contenu(model,image):
    # prédire l'objet le plus probable dans cette image
    prediction = model.predict(image)

    # afficher le résultat
    resultats = decode_predictions(prediction, top=3)
    resultats = resultats[0]
    for id_class, nom, proba in resultats:
        print(f'I detect a {nom} with a probability of {proba}')

def afficher_image(image_path):
    image = load_img(image_path)
    plt.imshow(image)
    plt.axis('off')
    plt.show()



class Evaluator(object):

    def __init__(self, eval_loss_and_grads):
        self.loss_value = None
        self.grads_values = None
        self.eval_loss_and_grads = eval_loss_and_grads

    def loss(self, x):
        assert self.loss_value is None
        loss_value, grad_values = self.eval_loss_and_grads(x)
        self.loss_value = loss_value
        self.grad_values = grad_values
        return self.loss_value

    def grads(self, x):
        assert self.loss_value is not None
        grad_values = np.copy(self.grad_values)
        self.loss_value = None
        self.grad_values = None
        return grad_values


class Faussaire:

    def __init__(self, base_image_path, chemin_style_image,
                 distance_entre_contenu,
                 evaluer_coherence,
                 distance_entre_style,
                 nb_iterations=5,
                 poids_variations=1, poids_style=1, poids_contenu=0.025):

        self.nb_iterations = nb_iterations
        self.ponderation_coherence = poids_variations
        self.ponderation_style = poids_style
        self.ponderation_contenu = poids_contenu
        self.distance_entre_contenu = distance_entre_contenu


        width, height = load_img(base_image_path).size
        self.nombre_lignes = 200
        self.nombre_colonnes = int(width * self.nombre_lignes / height)
        self.evaluer_coherence = lambda x : evaluer_coherence(x, self.nombre_lignes, self.nombre_colonnes)
        self.distance_entre_style = lambda x1, x2 : distance_entre_style(x1, x2, self.nombre_lignes*self.nombre_colonnes)


        # get tensor representations of our images
        base_image = ts.variable(self.preparer_image(base_image_path))
        style_image = ts.variable(self.preparer_image(chemin_style_image))

        # this will contain our generated image
        combinaison_image = ts.placeholder((1, self.nombre_lignes, self.nombre_colonnes, 3))

        # combine the 3 images into a single Keras tensor
        input_tensor = ts.concatenate([base_image,
                                          style_image,
                                          combinaison_image], axis=0)

        # build the VGG19 network with our 3 images as input
        # the reseau_neurones will be loaded with pre-trained ImageNet weights
        reseau_neurones = vgg19.VGG19(input_tensor=input_tensor,
                            weights='imagenet', include_top=False)
        print('Model loaded.')
        print(reseau_neurones.summary())

        # get the symbolic outputs of each "key" layer (we gave them unique names).
        couches_du_reseau = dict([(layer.name, layer.output) for layer in reseau_neurones.layers])

        # compute the neural style loss
        # first we need to define 4 util functions

        # the gram matrix of an image tensor (feature-wise outer product)

        # combine these loss functions into a single scalar
        cout = ts.variable(0.0)
        couche_analyse_image = couches_du_reseau['block5_conv2']
        caracteristiques_src_image = couche_analyse_image[0, :, :, :]
        caracteristiques_res_image = couche_analyse_image[2, :, :, :]
        cout += self.ponderation_contenu * self.distance_entre_contenu(caracteristiques_src_image,
                                                                       caracteristiques_res_image)

        couches_analyse_image = ['block1_conv1', 'block2_conv1',
                          'block3_conv1', 'block4_conv1',
                          'block5_conv1']
        for layer_name in couches_analyse_image:
            couche_analyse_image = couches_du_reseau[layer_name]
            caracteristiques_style_image = couche_analyse_image[1, :, :, :]
            caracteristiques_res_image = couche_analyse_image[2, :, :, :]
            sl = self.distance_entre_style(caracteristiques_style_image, caracteristiques_res_image)
            cout += (self.ponderation_style / len(couches_analyse_image)) * sl
        cout += self.ponderation_coherence * self.evaluer_coherence(combinaison_image)

        # get the gradients of the generated image wrt the loss
        grads = ts.gradients(cout, combinaison_image)

        outputs = [cout]
        if isinstance(grads, (list, tuple)):
            outputs += grads
        else:
            outputs.append(grads)

        self.f_outputs = ts.function([combinaison_image], outputs)


    def preparer_image(self, image_path):
        img = load_img(image_path, target_size=(self.nombre_lignes, self.nombre_colonnes))
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = vgg19.preprocess_input(img)
        return img

    def vers_image_normale(self, x):
        x = x.reshape((self.nombre_lignes, self.nombre_colonnes, 3))
        # Remove zero-center by mean pixel
        x[:, :, 0] += 103.939
        x[:, :, 1] += 116.779
        x[:, :, 2] += 123.68
        # 'BGR'->'RGB'
        x = x[:, :, ::-1]
        x = np.clip(x, 0, 255).astype('uint8')
        return x


    def evaluer_cout_et_gradient(self, img):
        img = img.reshape((1, self.nombre_lignes, self.nombre_colonnes, 3))
        outs = self.f_outputs([img])
        loss_value = outs[0]
        if len(outs[1:]) == 1:
            grad_values = outs[1].flatten().astype('float64')
        else:
            grad_values = np.array(outs[1:]).flatten().astype('float64')
        return loss_value, grad_values


    def lancer(self, chemin_src_image, res_name):

        evaluator = Evaluator(self.evaluer_cout_et_gradient)

        src_image = self.preparer_image(chemin_src_image)

        for i in range(self.nb_iterations):
            print(f'{i+1}/{self.nb_iterations}')
            src_image, min_val, info = fmin_l_bfgs_b(evaluator.loss, src_image.flatten(),
                                             fprime=evaluator.grads, maxfun=20)
            print('Current loss value:', min_val)
            # save current generated image
            img = self.vers_image_normale(src_image.copy())
            fname = f'{res_name}_{i}.png'
            save_img(fname, img)
            print('Image saved as', fname)
